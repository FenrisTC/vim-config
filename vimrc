"" My initial .vimrc
"" WIP
"" Author: Sebastian Schüller
"" Thu 26 Dec 20:15:03 CET 2013

""""""""""""""""""""""""""""""""""""""""
" General Settings
""""""""""""""""""""""""""""""""""""""""
" Set nocomatpible as soon as needed
set nocompatible

" Turn filetype off (requriement for vundle)
filetype off

" Set filetype plugins
filetype plugin on
filetype indent on


" Set new mapleader
let g:mapleader = "\<Space>"
let g:maplocalleader = "\<Space>"

" Load vim-plug
call plug#begin('~/.vim/plugged')

"""""""""""""""""""""""""""""""""""""""""
" Plugs
"""""""""""""""""""""""""""""""""""""""""

"" Extra colorschemes
" Vividchalk
Plug 'tpope/vim-vividchalk'
Plug 'sjl/badwolf'
Plug 'morhetz/gruvbox'
Plug 'Pychimp/vim-luna'
Plug 'Pychimp/vim-sol'
Plug 'junegunn/seoul256.vim'
Plug 'chriskempson/base16-vim'
""

" Use NerdTree
Plug 'scrooloose/nerdtree'

" Use NerdComment
Plug 'scrooloose/nerdcommenter'

" Latex lib
Plug 'xuhdev/vim-latex-live-preview'

" Use fugitive
Plug 'tpope/vim-fugitive'

" And gitgutter
Plug 'airblade/vim-gitgutter'

" Use airline
Plug 'bling/vim-airline'

" Load vim-misc for vim-notes
Plug 'xolox/vim-misc'

" Use vim-notes
Plug 'xolox/vim-notes'

" Try vim org mode
Plug 'jceb/vim-orgmode'

" Use sudo mode with user profile
Plug 'sudo.vim'

" Use CtrlP fuzzy search 
Plug 'kien/ctrlp.vim'

Plug 'nathanaelkane/vim-indent-guides'

" Jai syntax for fun!
Plug 'jansedivy/jai.vim'

" Use Tabular
Plug 'godlygeek/tabular'

" Try Gundo
Plug 'sjl/gundo.vim'

" Be able to apply a syntax to a 
" part of a buffer
Plug 'vim-scripts/SyntaxRange'

" Edit only specific regions
Plug 'chrisbra/NrrwRgn'

" Syntax for cpp(11)
Plug 'octol/vim-cpp-enhanced-highlight'

" Try Unite
Plug 'Shougo/unite.vim'

" end plugs
call plug#end()

"set rtp+=/usr/lib/python-2.7/site-packages/powerline/bindings/vim
""""""""""""""""""""""""""""""""""""""""
" UI Settings
""""""""""""""""""""""""""""""""""""""""
" Configure backspace to usual beahviour
set backspace=eol,start,indent
" Add left & right line-movment wrapping
set whichwrap+=<,>,h,l

" Use bash-like tab completion
set wildmenu

" Ignore certain filetypes
set wildignore=*.o,*.a,*.~,*.pyc

"" Search behaviuor
" Don't search case sensitive
set ignorecase

" Try a smartcase search
set smartcase

" Do incemental search
set incsearch

"Highlight search
set nohlsearch
""

" Don't do unnecessary redraws
set lazyredraw

" Show ruler position as default
set ruler

" Show line numbers
set number
set relativenumber

" Don't wrap long lines
"set nowrap
set scrolloff=5
"set sidescroll=1
"set sidescrolloff=10

" Split in more intuitive ways
set splitbelow
set splitright


" Highlight trailing stuff
set list listchars=tab:»·,trail:·

" show the cursorline
set cursorline

""""""""""""""""""""""""""""""""""""""""
" Input settings
""""""""""""""""""""""""""""""""""""""""
" Set keyboard input timeout to allow
" better usage of the leader key
set timeout
set timeoutlen=2000
set nottimeout
set ttimeoutlen=0

" Use mouse for scrolling
set mouse=a

""""""""""""""""""""""""""""""""""""""""
" Undo rules
""""""""""""""""""""""""""""""""""""""""
" Turn off backup
set nobackup

" Turn off swap file
set nowb
set noswapfile

""""""""""""""""""""""""""""""""""""""""
" Tabulator settings
""""""""""""""""""""""""""""""""""""""""
" Usually you want to use spaces instead
" of tabs
set expandtab

" Use smart tab functionality
set smarttab

" Tabwidth:=4 spaces
set tabstop=4
set shiftwidth=4

"" Set indentation options
" Enable auto indentation
set ai

" Use vims smart indentation feature
" (turn off if it's bothering)
set si
""

""""""""""""""""""""""""""""""""""""""""
" Statusline settings
""""""""""""""""""""""""""""""""""""""""

set tabline=1
set laststatus=2
"set statusline+=%{exists('g:loaded_fugitive')?fugitive#statusline():''}


""""""""""""""""""""""""""""""""""""""""
" Colors and Themes"
""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable

" Set colorscheme
colorscheme gruvbox
set background=dark

" Enable 256 colors
set t_Co=256

"" Extra Options for vim-qt
if has("gui_running")
    " set different colorscheme
    colorscheme base16-embers
    " remove Toolbar
    set guioptions=
    " configure the label shown in tabs
    set guitablabel=
    " set a guifont
    set guifont=Source\ Code\ Pro\ for\ Powerline\ 10
    " always use block cursor
    set guicursor=n-v-c-i:block-Cursor
    " never allow blinking
    set guicursor+=n-v-c-i:blinkon0
endif
""

"""""""""""""""""""""""""""""""""""""""""
" Plugin Settings
"
""""""""""""""""""""""""""""""""""""""""

"" vim-latex settings

" set compiler to pdf
"let g:Tex_DefaultTargetFormat = 'pdf'
"let g:Tex_MultipleCompileFormats='pdf, aux'

" set pdf viewer
"let g:Tex_ViewRule_pdf = 'okular'
"let g:vimtex_fold_enable=0
"let g:tex_flavor = 'latex'
""

""
" airline settings
let g:airline_powerline_fonts = 1
""

""
" CtrlP settings
let g:ctrlp_max_depth = 40
let g:ctrlp_max_files = 50000
let g:ctrlp_working_path_mode = 'a'


"" Color scheme settings
" badwolf
"let g:badwolf_darkgutter=0
""

"""""""""""""""""""""""""""""""""""""""""
" Extra Functions
"
"""""""""""""""""""""""""""""""""""""""""

if executable('ag')
    set grepprg=ag\ --nogroup\ --nocolor
    let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif

"""""""""""""""""""""""""""""""""""""""""
" Remaps and Abbrevations
"
"""""""""""""""""""""""""""""""""""""""""

" Torture! Remove arrow keys
nnoremap <left> <nop>
inoremap <left> <nop>
nnoremap <up> <nop>
inoremap <up> <nop>
nnoremap <right> <nop>
inoremap <right> <nop>
nnoremap <down> <nop>
inoremap <down> <nop>


" Faster writing of files
nnoremap <Leader>w :w<cr>
nnoremap <Leader>q :q<cr>
nnoremap <Leader>x :x<cr>
nnoremap <Leader>Q :q!<cr>

" Remove Ex Mode until you find a use for it
nnoremap Q <nop>

" Open NerdTree with ctrl f
"nnoremap <Leader>f :NERDTreeToggle<cr>

" *L*ist all open buffers with CtrlP
map <C-l> :CtrlPBuffer<cr>

" Edit vimrc
nnoremap <Leader>fed :vs ~/.vimrc<cr>
" And reload
nnoremap <Leader>fer :source ~/.vimrc<cr>

" Toggle relative numbers
nnoremap <Leader>tr :set number !<cr>:set relativenumber !<cr>
